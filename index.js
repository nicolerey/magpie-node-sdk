'use strict';

require('dotenv').config();

const axios = require('./src/utils/axios');
const { setStatementDescriptor } = require('./src/constants/magpie');

const cardLib = require('./src/libs/cards');
const chargeLib = require('./src/libs/charges');
const customerLib = require('./src/libs/customers');

/**
 *
 * @param {string} key Magpie API key
 * @param {string} statementDesc Magpie bill statement description
 */
module.exports = (key, statementDesc) => {
  const apiKey = key;

  axios.createInstance(apiKey);
  setStatementDescriptor(statementDesc);

  return {
    card: {
      create: cardLib.create,
      get: cardLib.get,
    },
    charge: {
      chargeCard: chargeLib.chargeCard,
      get: chargeLib.get,
      capture: chargeLib.capture,
      refund: chargeLib.refund,
    },
    customer: {
      create: customerLib.create,
      get: customerLib.get,
      remove: customerLib.remove,
      addCard: customerLib.addCard,
      removeCard: customerLib.removeCard,
    }
  };
};
