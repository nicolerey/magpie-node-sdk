'use strict';

let MAGPIE_STATEMENT_DESC = '';

exports.MAGPIE_URL = 'https://api.magpie.im/v1';

exports.setStatementDescriptor = (statementDesc) => {
  MAGPIE_STATEMENT_DESC = statementDesc || 'Magpie.IM';
};
exports.getStatementDescriptor = () => MAGPIE_STATEMENT_DESC;
