const { expect } = require('chai');
const { stub } = require('sinon');

const axiosUtil = require('../utils/axios');

const customers = require('./customers');

const EXCEPTION = 'Axios exception';

describe('customers', () => {
  describe('create', () => {
    it('SUCCESS - should call create function', async () => {
      const postData = {
        id: 'cus_MTQ3ODRlNjc5YTQ2',
        object: 'customer',
        account_balance: 0,
        created: 1459571613,
        currency: null,
        default_source: null,
        delinquent: false,
        description: 'Juan de la Cruz (Created by PayTerminal)',
        email: 'juandelacruz@pinoymail.com',
        sources: [],
      };
      const email = 'test-sample@gm.com';
      const description = 'Test Sample';

      const postStub = stub().resolves({ data: postData });
      const getAxiosInstanceStub = stub(axiosUtil, 'getAxiosInstance')
        .returns({ post: postStub });

      try {
        const resp = await customers.create(email, description);

        expect(postStub.calledOnceWith('/customers', { email, description })).to.equal(true);
        expect(resp).to.deep.equal(postData);
      } catch(err) {
        expect(true).to.equal(false);
      }

      getAxiosInstanceStub.restore();
    });

    it('ERROR - should call create function', async () => {
      const getAxiosInstanceStub = stub(axiosUtil, 'getAxiosInstance')
        .returns({ post: stub().rejects(EXCEPTION) });

      try {
        await customers.create();
        expect(true).to.equal(false);
      } catch(err) {
        expect(err.name).to.equal(EXCEPTION);
        expect(true).to.equal(true);
      }

      getAxiosInstanceStub.restore();
    });
  });

  describe('get', () => {
    it('should call get function and return data', async () => {
      const getData = {
        id: 'cus_MTQ3ODRlNjc5YTQ2',
        object: 'customer',
        account_balance: 0,
        created: 1459571613,
        currency: null,
        default_source: null,
        delinquent: false,
        description: 'Juan de la Cruz (Created by PayTerminal)',
        email: 'juandelacruz@pinoymail.com',
        sources: [],
      };
      const id = 123;

      const getStub = stub().resolves({ data: getData });
      const getAxiosInstanceStub = stub(axiosUtil, 'getAxiosInstance')
        .returns({ get: getStub });

      try {
        const resp = await customers.get(id);

        expect(getStub.calledOnceWith(`/customers/${id}`)).to.equal(true);
        expect(resp).to.deep.equal(getData);
      } catch(err) {
        expect(true).to.equal(false);
      }

      getAxiosInstanceStub.restore();
    });

    it('should call get function with exception', async () => {
      const getAxiosInstanceStub = stub(axiosUtil, 'getAxiosInstance')
        .returns({ get: stub().rejects(EXCEPTION) });

      try {
        await customers.get();
        expect(true).to.equal(false);
      } catch(err) {
        expect(err.name).to.equal(EXCEPTION);
        expect(true).to.equal(true);
      }

      getAxiosInstanceStub.restore();
    });
  });

  describe('addCard', () => {
    it('should call get function and return data', async () => {
      const putData = {
        id: 'cus_MTQ3ODRlNjc5YTQ2',
        object: 'customer',
        account_balance: 0,
        created: 1459571613,
        currency: null,
        default_source: null,
        delinquent: false,
        description: 'Juan de la Cruz (Created by PayTerminal)',
        email: 'juandelacruz@pinoymail.com',
        sources: [],
      };
      const id = 123;
      const tokenId = 456;

      const putStub = stub().resolves({ data: putData });
      const getAxiosInstanceStub = stub(axiosUtil, 'getAxiosInstance')
        .returns({ put: putStub });

      try {
        const resp = await customers.addCard(id, tokenId);

        expect(putStub.calledOnceWith(`/customers/${id}`, { source: tokenId })).to.equal(true);
        expect(resp).to.deep.equal(putData);
      } catch(err) {
        expect(true).to.equal(false);
      }

      getAxiosInstanceStub.restore();
    });

    it('should call get function with exception', async () => {
      const getAxiosInstanceStub = stub(axiosUtil, 'getAxiosInstance')
        .returns({ put: stub().rejects(EXCEPTION) });

      try {
        await customers.addCard();
        expect(true).to.equal(false);
      } catch(err) {
        expect(err.name).to.equal(EXCEPTION);
        expect(true).to.equal(true);
      }

      getAxiosInstanceStub.restore();
    });
  });

  describe('remove', () => {
    it('should call get function and return data', async () => {
      const id = 123;
      const deleteData = {
        deleted: true,
        id,
      };

      const deleteStub = stub().resolves({ data: deleteData });
      const getAxiosInstanceStub = stub(axiosUtil, 'getAxiosInstance')
        .returns({ delete: deleteStub });

      try {
        const resp = await customers.remove(id);

        expect(deleteStub.calledOnceWith(`/customers/${id}`)).to.equal(true);
        expect(resp).to.deep.equal(deleteData);
      } catch(err) {
        expect(true).to.equal(false);
      }

      getAxiosInstanceStub.restore();
    });

    it('should call get function with exception', async () => {
      const getAxiosInstanceStub = stub(axiosUtil, 'getAxiosInstance')
        .returns({ delete: stub().rejects(EXCEPTION) });

      try {
        await customers.remove();
        expect(true).to.equal(false);
      } catch(err) {
        expect(err.name).to.equal(EXCEPTION);
        expect(true).to.equal(true);
      }

      getAxiosInstanceStub.restore();
    });
  });

  describe('removeCard', () => {
    it('should call get function and return data', async () => {
      const id = 123;
      const tokenId = 456;
      const deleteData = {
        deleted: true,
        id,
      };

      const deleteStub = stub().resolves({ data: deleteData });
      const getAxiosInstanceStub = stub(axiosUtil, 'getAxiosInstance')
        .returns({ delete: deleteStub });

      try {
        const resp = await customers.removeCard(id, tokenId);

        expect(deleteStub.calledOnceWith(`/customers/${id}/sources/${tokenId}`)).to.equal(true);
        expect(resp).to.deep.equal(deleteData);
      } catch(err) {
        expect(true).to.equal(false);
      }

      getAxiosInstanceStub.restore();
    });

    it('should call get function with exception', async () => {
      const getAxiosInstanceStub = stub(axiosUtil, 'getAxiosInstance')
        .returns({ delete: stub().rejects(EXCEPTION) });

      try {
        await customers.removeCard();
        expect(true).to.equal(false);
      } catch(err) {
        expect(err.name).to.equal(EXCEPTION);
        expect(true).to.equal(true);
      }

      getAxiosInstanceStub.restore();
    });
  });
});
