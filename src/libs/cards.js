
const axiosUtil = require('../utils/axios');

/**
 *
 * @param {Object} data Card data object
 * @param {string} data.name Card holders name - required
 * @param {string} data.number Card number - required
 * @param {number} data.exp_month Card expiration month [1-12] - required
 * @param {number} data.exp_year Card expiration year - required
 * @param {string} data.cvc Card cvc - required
 * @param {string} data.address_city Card address_city
 * @param {string} data.address_country Card address_country
 * @param {string} data.address_line1 Card address_line1
 * @param {string} data.address_line2 Card address_line2
 * @param {string} data.address_state Card address_state
 * @param {string} data.address_zip Card address_zip
 */
exports.create = async (data) => {
  const axios = axiosUtil.getAxiosInstance();
  const resp = await axios.post('/tokens', { card: data });
  return resp.data;
};

/**
 *
 * @param {number} id Card token ID
 */
exports.get = async (id) => {
  const axios = axiosUtil.getAxiosInstance();
  const resp = await axios.get(`/tokens/${id}`);
  return resp.data;
};
