const { expect } = require('chai');
const { stub } = require('sinon');

const axiosUtil = require('../utils/axios');

const cards = require('./cards');

const EXCEPTION = 'Axios exception';

describe('cards', () => {
  describe('create', () => {
    it('SUCCESS - should call create function', async () => {
      const postData = {
        id: 'tok_MTQ1ODAyOczNDJkj',
        object: 'token',
        card: {
          id: 'card_MTQ1OTVhYzIyOGIy',
          object: 'card',
        },
      };
      const cardData = {
        name: 'Juan de la Cruz',
        number: '4242424242424242',
        exp_month: 10,
        exp_year: 2020,
        cvc: '123',
      };

      const postStub = stub().resolves({ data: postData });
      const getAxiosInstanceStub = stub(axiosUtil, 'getAxiosInstance')
        .returns({ post: postStub });

      try {
        const resp = await cards.create(cardData);

        expect(postStub.calledOnceWith('/tokens', { card: cardData })).to.equal(true);
        expect(resp).to.deep.equal(postData);
      } catch(err) {
        expect(true).to.equal(false);
      }

      getAxiosInstanceStub.restore();
    });

    it('ERROR - should call create function', async () => {
      const getAxiosInstanceStub = stub(axiosUtil, 'getAxiosInstance')
        .returns({ post: stub().rejects(EXCEPTION) });

      try {
        await cards.create();
        expect(true).to.equal(false);
      } catch(err) {
        expect(err.name).to.equal(EXCEPTION);
        expect(true).to.equal(true);
      }

      getAxiosInstanceStub.restore();
    });
  });

  describe('get', () => {
    it('should call get function and return data', async () => {
      const getData = {
        id: 'tok_MTQ1ODAyOczNDJkj',
        object: 'token',
        card: {
          id: 'card_MTQ1OTVhYzIyOGIy',
          object: 'card',
        },
      };
      const id = 123;

      const getStub = stub().resolves({ data: getData });
      const getAxiosInstanceStub = stub(axiosUtil, 'getAxiosInstance')
        .returns({ get: getStub });

      try {
        const resp = await cards.get(id);

        expect(getStub.calledOnceWith(`/tokens/${id}`)).to.equal(true);
        expect(resp).to.deep.equal(getData);
      } catch(err) {
        expect(true).to.equal(false);
      }

      getAxiosInstanceStub.restore();
    });

    it('should call get function with exception', async () => {
      const getAxiosInstanceStub = stub(axiosUtil, 'getAxiosInstance')
        .returns({ get: stub().rejects(EXCEPTION) });

      try {
        await cards.get();
        expect(true).to.equal(false);
      } catch(err) {
        expect(err.name).to.equal(EXCEPTION);
        expect(true).to.equal(true);
      }

      getAxiosInstanceStub.restore();
    });
  });
});
