const { expect } = require('chai');
const { stub } = require('sinon');

const axiosUtil = require('../utils/axios');
const magpieConst = require('../constants/magpie');

const charges = require('./charges');

const EXCEPTION = 'Axios exception';

describe('charges', () => {
  describe('chargeCard', () => {
    it('SUCCESS - should call chargeCard function WITH statement_descriptor', async () => {
      const amount = 50000;
      const postData = {
        id: 'ch_MTQ3OODJlOTE4Zjll',
        object: 'charge',
        amount: amount * 100,
        amount_refunded: 0,
        captured: true,
        created: 1477884972,
        currency: 'PHP',
        status: 'succeeded',
        statement_descriptor: 'Pet Shop Inc.',
        description: 'Pet food and supplies',
        livemode: false,
        source: {
          id: 'card_MTQ5MGJjODRZlODli',
          object: 'card',
        },
      };
      const chargeData = {
        amount,
        currency: 'php',
        source: 'tok_MTQ1ODAyOczNDJkj',
        description: 'Pet food and other supplies',
        statement_descriptor: 'Pet Shop Inc',
        capture: true,
      };

      const postStub = stub().resolves({ data: postData });
      const getAxiosInstanceStub = stub(axiosUtil, 'getAxiosInstance')
        .returns({ post: postStub });

      try {
        const resp = await charges.chargeCard(chargeData);

        expect(postStub.calledOnceWith('/charges', {
          ...chargeData,
          amount: amount * 100,
        })).to.equal(true);
        expect(resp).to.deep.equal({ ...postData, amount });
      } catch(err) {
        expect(true).to.equal(false);
      }

      getAxiosInstanceStub.restore();
    });

    it('SUCCESS - should call chargeCard function WITHOUT statement_descriptor', async () => {
      const statementDescriptor = 'sample description';
      const amount = 50000;
      const postData = {
        id: 'ch_MTQ3OODJlOTE4Zjll',
        object: 'charge',
        amount: amount * 100,
        amount_refunded: 0,
        captured: true,
        created: 1477884972,
        currency: 'PHP',
        status: 'succeeded',
        statement_descriptor: 'Pet Shop Inc.',
        description: 'Pet food and supplies',
        livemode: false,
        source: {
          id: 'card_MTQ5MGJjODRZlODli',
          object: 'card',
        },
      };
      const chargeData = {
        amount,
        currency: 'php',
        source: 'tok_MTQ1ODAyOczNDJkj',
        description: 'Pet food and other supplies',
        capture: true,
      };

      const postStub = stub().resolves({ data: postData });
      const getStatementDescriptorStub = stub(magpieConst, 'getStatementDescriptor').returns(statementDescriptor);
      const getAxiosInstanceStub = stub(axiosUtil, 'getAxiosInstance')
        .returns({ post: postStub });

      try {
        const resp = await charges.chargeCard(chargeData);

        expect(postStub.calledOnceWith('/charges', {
          ...chargeData,
          amount: amount * 100,
          statement_descriptor: statementDescriptor,
        })).to.equal(true);
        expect(resp).to.deep.equal({ ...postData, amount });
      } catch(err) {
        expect(true).to.equal(false);
      }

      getAxiosInstanceStub.restore();
      getStatementDescriptorStub.restore();
    });

    it('ERROR - should call chargeCard function', async () => {
      const getAxiosInstanceStub = stub(axiosUtil, 'getAxiosInstance')
        .returns({ post: stub().rejects(EXCEPTION) });

      try {
        await charges.chargeCard({});
        expect(true).to.equal(false);
      } catch(err) {
        expect(err.name).to.equal(EXCEPTION);
        expect(true).to.equal(true);
      }

      getAxiosInstanceStub.restore();
    });
  });

  describe('get', () => {
    it('SUCCESS - should call get function', async () => {
      const amount = 50000;
      const getData = {
        id: 'ch_MTQ3OODJlOTE4Zjll',
        object: 'charge',
        amount: amount * 100,
        amount_refunded: 0,
        captured: true,
        created: 1477884972,
        currency: 'PHP',
        status: 'succeeded',
        statement_descriptor: 'Pet Shop Inc.',
        description: 'Pet food and supplies',
        livemode: false,
        source: {
          id: 'card_MTQ5MGJjODRZlODli',
          object: 'card',
        },
      };
      const id = 123;

      const getStub = stub().resolves({ data: getData });
      const getAxiosInstanceStub = stub(axiosUtil, 'getAxiosInstance')
        .returns({ get: getStub });

      try {
        const resp = await charges.get(id);

        expect(getStub.calledOnceWith(`/charges/${id}`)).to.equal(true);
        expect(resp).to.deep.equal({ ...getData, amount });
      } catch(err) {
        expect(true).to.equal(false);
      }

      getAxiosInstanceStub.restore();
    });

    it('ERROR - should call get function', async () => {
      const getAxiosInstanceStub = stub(axiosUtil, 'getAxiosInstance')
        .returns({ get: stub().rejects(EXCEPTION) });

      try {
        await charges.get();
        expect(true).to.equal(false);
      } catch(err) {
        expect(err.name).to.equal(EXCEPTION);
        expect(true).to.equal(true);
      }

      getAxiosInstanceStub.restore();
    });
  });

  describe('capture', () => {
    it('SUCCESS - should call capture function', async () => {
      const amount = 50000;
      const postData = {
        id: 'ch_MTQ3OODJlOTE4Zjll',
        object: 'charge',
        amount: amount * 100,
        amount_refunded: 0,
        captured: true,
        created: 1477884972,
        currency: 'PHP',
        status: 'succeeded',
        statement_descriptor: 'Pet Shop Inc.',
        description: 'Pet food and supplies',
        livemode: false,
        source: {
          id: 'card_MTQ5MGJjODRZlODli',
          object: 'card',
        },
      };
      const id = 123;

      const postStub = stub().resolves({ data: postData });
      const getAxiosInstanceStub = stub(axiosUtil, 'getAxiosInstance')
        .returns({ post: postStub });

      try {
        const resp = await charges.capture(id, amount);

        expect(postStub.calledOnceWith(`/charges/${id}/capture`, {
          amount: amount * 100,
        })).to.equal(true);
        expect(resp).to.deep.equal({ ...postData, amount });
      } catch(err) {
        expect(true).to.equal(false);
      }

      getAxiosInstanceStub.restore();
    });

    it('ERROR - should call capture function', async () => {
      const getAxiosInstanceStub = stub(axiosUtil, 'getAxiosInstance')
        .returns({ post: stub().rejects(EXCEPTION) });

      try {
        await charges.capture();
        expect(true).to.equal(false);
      } catch(err) {
        expect(err.name).to.equal(EXCEPTION);
        expect(true).to.equal(true);
      }

      getAxiosInstanceStub.restore();
    });
  });

  describe('refund', () => {
    it('SUCCESS - should call refund function', async () => {
      const amount = 50000;
      const postData = {
        id: 'ch_MTQ3OODJlOTE4Zjll',
        object: 'charge',
        amount: amount * 100,
        amount_refunded: 0,
        captured: true,
        created: 1477884972,
        currency: 'PHP',
        status: 'succeeded',
        statement_descriptor: 'Pet Shop Inc.',
        description: 'Pet food and supplies',
        livemode: false,
        source: {
          id: 'card_MTQ5MGJjODRZlODli',
          object: 'card',
        },
      };
      const id = 123;

      const postStub = stub().resolves({ data: postData });
      const getAxiosInstanceStub = stub(axiosUtil, 'getAxiosInstance')
        .returns({ post: postStub });

      try {
        const resp = await charges.refund(id, amount);

        expect(postStub.calledOnceWith(`/charges/${id}/refund`, {
          amount: amount * 100,
        })).to.equal(true);
        expect(resp).to.deep.equal({ ...postData, amount });
      } catch(err) {
        expect(true).to.equal(false);
      }

      getAxiosInstanceStub.restore();
    });

    it('ERROR - should call refund function', async () => {
      const getAxiosInstanceStub = stub(axiosUtil, 'getAxiosInstance')
        .returns({ post: stub().rejects(EXCEPTION) });

      try {
        await charges.refund();
        expect(true).to.equal(false);
      } catch(err) {
        expect(err.name).to.equal(EXCEPTION);
        expect(true).to.equal(true);
      }

      getAxiosInstanceStub.restore();
    });
  });
});
