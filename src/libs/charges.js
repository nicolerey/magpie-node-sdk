
const axiosUtil = require('../utils/axios');
const magpieConst = require('../constants/magpie');

const computeAmount = (charging) => ({
  ...charging,
  amount: charging.amount / 100,
});

/**
 *
 * @param {Object} data Charging details
 * @param {string} data.source Card token to be charged - required
 * @param {number} data.amount Amount to charge - required
 * @param {string} data.currency Currency
 * @param {string} data.description Charge description
 * @param {string} data.statement_descriptor Card statement description
 * @param {boolean} data.capture Whether or not to immediately capture the charge
 */
exports.chargeCard = async (data) => {
  // Check if no statement description is given
  if (!data.statement_descriptor) {
    data.statement_descriptor = magpieConst.getStatementDescriptor();
  }

  // Convert amount to integer
  data.amount = data.amount * 100;

  const axios = axiosUtil.getAxiosInstance();
  const resp = await axios.post('/charges', data);
  return computeAmount(resp.data);
};

/**
 *
 * @param {number} id Charge ID
 */
exports.get = async (id) => {
  const axios = axiosUtil.getAxiosInstance();
  const resp = await axios.get(`/charges/${id}`);
  return computeAmount(resp.data);
};

/**
 *
 * @param {string} id Charge ID
 * @param {number} amount Charge amount
 */
exports.capture = async (id, amount) => {
  const axios = axiosUtil.getAxiosInstance();
  const resp = await axios.post(`/charges/${id}/capture`, { amount: amount * 100 });
  return computeAmount(resp.data);
};

/**
 *
 * @param {string} id Charge ID
 * @param {number} amount Refund amount
 */
exports.refund = async (id, amount) => {
  const axios = axiosUtil.getAxiosInstance();
  const resp = await axios.post(`/charges/${id}/refund`, { amount: amount * 100 });
  return computeAmount(resp.data);
};
