
const axiosUtil = require('../utils/axios');

/**
 *
 * @param {string} email Customer email
 * @param {string} description Customer description
 */
exports.create = async (email, description) => {
  const axios = axiosUtil.getAxiosInstance();
  const resp = await axios.post('/customers', { email, description });
  return resp.data;
};

/**
 *
 * @param {string} id Customer ID
 */
exports.get = async (id) => {
  const axios = axiosUtil.getAxiosInstance();
  const resp = await axios.get(`/customers/${id}`);
  return resp.data;
};

/**
 *
 * @param {string} id Customer ID
 * @param {string} tokenId Card token ID
 */
exports.addCard = async (id, tokenId) => {
  const axios = axiosUtil.getAxiosInstance();
  const resp = await axios.put(`/customers/${id}`, { source: tokenId });
  return resp.data;
};

/**
 *
 * @param {string} id Customer ID
 */
exports.remove = async (id) => {
  const axios = axiosUtil.getAxiosInstance();
  const resp = await axios.delete(`/customers/${id}`);
  return resp.data;
};

/**
 *
 * @param {string} id Customer ID
 * @param {string} tokenId Card token ID
 */
exports.removeCard = async (id, tokenId) => {
  const axios = axiosUtil.getAxiosInstance();
  const resp = await axios.delete(`/customers/${id}/sources/${tokenId}`);
  return resp.data;
};
