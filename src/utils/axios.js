'use strict';

const axios = require('axios');

const { MAGPIE_URL } = require('../constants/magpie');

// Axios instance
let instance = null;

exports.createInstance = (apiKey) => {
  const baseUrl = MAGPIE_URL || '';

  // Format key for authorization
  const data = `${apiKey}:`;
  const authorization = new Buffer(data).toString('base64');

  const headers = {
    'Content-Type': 'application/json',
    Accept: 'application/json',
    Authorization: `Basic ${authorization}`,
  };

  instance = axios.create({
    baseURL: baseUrl,
    timeout: 60000,
    maxContentLength: 6000000,
    headers,
  });
};

exports.getAxiosInstance = () => instance;
